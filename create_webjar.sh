#!/bin/bash

echo "Creating WebJar..."

# If the dist directory is empty, throw an error
if [ ! "$(ls -A ./dist)" ]; then
    echo "Error: dist directory is empty. Please run 'npm run build' first."
    exit 1
fi

# Get the project name from the package.json file
export PROJECT_NAME=$(grep -o '"name": *"[^"]*"' package.json | awk -F'"' '{print $4}')
echo "Project Name: $PROJECT_NAME"

# Get the current version from the package.json file
export PROJECT_VERSION=$(grep -o '"version": *"[^"]*"' package.json | awk -F'"' '{print $4}')
echo "Current Version: $PROJECT_VERSION"

# Set the WebJar directory path based on the version
WEBJAR_DIR="./webjar/resources/webjars/$PROJECT_NAME/$PROJECT_VERSION"

# Create the versioned directory 
mkdir -p $WEBJAR_DIR

# Copy the built assets to the WebJar directory
cp -r ./dist/* $WEBJAR_DIR  

# Create the WebJar file
./gradlew createWebjar

# Remove the versioned directory
rm -R ./webjar