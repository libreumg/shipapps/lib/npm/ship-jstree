import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path'
import { fileURLToPath } from 'url'
import removeConsole from "vite-plugin-remove-console";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    removeConsole({
      includes: ["log", ],
    })
  ],
  base: "./",
  define: { 'process.env': {} },
  build: {
    lib: {
      entry: resolve(__dirname, 'src/ShipJsTree.ts'),
      name: 'ship-jstree',
      fileName: 'ship-jstree',
    },
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    },
    extensions: [
      '.js',
      '.json',
      '.jsx',
      '.mjs',
      '.ts',
      '.tsx',
      '.vue',
    ],
  },
})
