# ship-jstree
This library is a wrapper for the [jstree](https://www.jstree.com/) library. 

## Installation

## Webjar
This library is available as webjar a in the [public ship-nexus repository](https://packages.qihs.uni-greifswald.de/#browse/browse:ship-java:de%2Fship%2Fwebjar%2Ftree%2Fship-jstree).

To use it in your java project, add the following repository to your gradle build file:
```groovy
repositories {
  mavenLocal()
  mavenCentral()
  maven {
    url "https://packages.qihs.uni-greifswald.de/repository/ship-java/"
  }
}
```
and the following dependency (replace `0.0.4` with the latest version):
```groovy
dependencies {
  implementation 'de.ship.webjar.tree:ship-jstree:0.0.4'
}
```
Make sure to link the js and css files in your html file, for example if using thyymeleaf:
```html
<script type="module" th:src="@{/webjars/ship-jstree/0.0.4/ship-jstree.js}"></script>
<link rel="stylesheet" type="text/css" th:href="@{/webjars/ship-jstree/0.0.4/style.css}" />
```
Please note that the `type="module"` attribute is required for the js file to work.

## Usage
The library allows you to control the jstree with specific HTML form elements. The following example shows how to use the library with a filter field and buttons to collapse and to refresh the tree.
```html
<div class="d-flex mb-2">
    <select id="jstree-filter-select" placeholder="Filter" class="form-select"></select>
    <button id="jstree-collapse-btn" type="button" class="btn btn-outline-primary mx-2">
        <i class="fa-solid fa-down-left-and-up-right-to-center"></i>
    </button>
    <button id="jstree-refresh-btn" type="button" class="btn btn-outline-primary">
        <i class="fa-solid fa-rotate-right"></i>
    </button>
</div>
<div id="ship-jstree"></div>
```

To initialize the tree, you need to 
1. import the library (in a module script)
2. create a new instance of the `ShipJsTree` class
3. Call the `init()` method of the instance
```js
import ShipJsTree from '../webjars/ship-jstree/0.0.4/ship-jstree.js'

const shipJsTree = new ShipJsTree({
    treeId: 'ship-jstree',
    treeFilterId: 'jstree-filter-select',
    collapseBtnId: 'jstree-collapse-btn',
    refreshBtnId: 'jstree-refresh-btn',
    nodeTextProperty: 'translation',
    nodeIconClass: 'fas fa-folder text-warning',
    leafIconClass: 'fas fa-file text-secondary',
    showPath: true,
    saveStateInLocalStorage: true,

    backend: {
        baseUrl: "https://drossel.ship-med.uni-greifswald.de/SHIPDD",
        apiVersion: "v0.2",
        locale: "de",
        routes: {
            children: "/children",
            subtree: "/subtree",
            path: "/path",
            filter: "/filter"
        }
    }
})

$(async () => {
    await shipJsTree.init()

    // you can change the locale of the tree like that:
    $('#lang-select').on('change', (e) => {
        shipJsTree.changeLocale(e.target.value)
    })
});
```

## API
### Constructor
The constructor takes an object based on the following typescript type [ShipJsTreeOptions](https://gitlab.com/libreumg/shipapps/lib/npm/ship-jstree/-/blob/1f2b9b1429da9a18307c9ff9843dae0485d40554/src/types/ShipJsTree.ts#L5-32).

### Methods
Method | Description | Parameters | Return value
--- | --- | --- | ---
`init()` | Initializes the tree. | - | `Promise<void>`
`changeLocale(locale: string)` | Changes the locale of the tree. | [locale](https://gitlab.com/libreumg/shipapps/lib/npm/ship-jstree/-/blob/1f2b9b1429da9a18307c9ff9843dae0485d40554/src/types/ShipJsTree.ts#L3) | `void`
`(event: string, callback: Function)` | Registers a callback for a specific jstree event. | [event](https://www.jstree.com/api/#/?q=event),  `callback: Function` | `void`
`getPath()` | Returns the path of the currently selected node. (Separated by '`/`') | - | `string` or `undefined`

An up-to-date list of all available methods can be found in the [ShipJsTree class](https://gitlab.com/libreumg/shipapps/lib/npm/ship-jstree/-/blob/1f2b9b1429da9a18307c9ff9843dae0485d40554/src/ShipJsTree.ts#L22-69).
More methods will be added in the future.