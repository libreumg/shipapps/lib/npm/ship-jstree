import { watch } from "vue";
import pinia from "./pinia";

// Store
import { useConfigStore } from "@/stores/ConfigStore";
import { useTreeStore } from '@/stores/TreeStore';
const lsKey = "ship-jstree";

export default function localStorageHandler() {
  if (localStorage.getItem(lsKey)) getLocalStorageState();
  const configStore = useConfigStore();
  const treeStore = useTreeStore();

  watch(
    () => configStore.getOptions()?.saveStateInLocalStorage,
    () => {
      if (configStore.getOptions()?.saveStateInLocalStorage) {
        localStorage.setItem(lsKey, JSON.stringify(pinia.state.value));
      } else {
        localStorage.removeItem(lsKey);
      }
    }
  );

  watch(
    () => pinia.state,
    () => {
      if (!configStore.getOptions()?.saveStateInLocalStorage) return
      if (!treeStore.isInitialized()) return
      localStorage.setItem(lsKey, JSON.stringify(pinia.state.value));
    },
    { deep: true }
  );
}

function getLocalStorageState() {
  if (!localStorage.getItem(lsKey)) return;
  const piniaState = localStorage.getItem(lsKey) ?? "";
  pinia.state.value = JSON.parse(piniaState);
}
