/**
 * plugins/index.ts
 *
 * Automatically included in `./src/main.ts`
 */

// Plugins
import pinia from "./pinia";
import localStorageHandler from "./localStorageHandler";

// Types
import type { App } from "vue";

export function registerPlugins(app: App) {
  app.use(pinia);
  app.use(localStorageHandler);
}
