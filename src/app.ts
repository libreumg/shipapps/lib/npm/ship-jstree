
import ShipJsTree from './ShipJsTree'
import backendConfig from '../backend-config.json'

const shipJsTree = new ShipJsTree({
    treeId: 'jstree-wrapper',
    treeFilterId: 'jstree-filter-select',
    collapseBtnId: 'jstree-collapse-btn',
    refreshBtnId: 'jstree-refresh-btn',
    nodeTextProperty: (node: any) => node.translation + ' (' + node.uniqueName + ')',
    nodeIconClass: 'fas fa-folder text-warning',
    leafIconClass: 'fas fa-file text-secondary',
    showPath: true,
    // saveStateInLocalStorage: false,
    saveStateInLocalStorage: true,

    backend: backendConfig as any
})

$(async () => {
    await shipJsTree.init()

    $('#lang-select').on('change', (e: any) => {
        shipJsTree.changeLocale(e.target.value)
    })

    $('#delete-btn').on('click', async () => {
        await shipJsTree.deleteState()
    })

    // await shipJsTree.deleteState()


    // await shipJsTree.selectNode('s0.int')

    await shipJsTree.selectNode('x0.bloodpre', true)
    
    // console.log("tree", shipJsTree.getTree())

    // setTimeout(async () => {
    //     await shipJsTree.selectNode('s0.int', true)
    //     // await shipJsTree.selectNode('x0.bloodpre')
    //     // console.log("tree 2", shipJsTree.getSelectedNode())

    //     // shipJsTree.deselectNode();

    //     console.log("tree 3", shipJsTree.getSelectedNode())
    //     // shipJsTree.getSelectedNode()

    // }, 3000)

    // shipJsTree.on('changed.jstree', (_: any, data: any) => {
    //     console.log("changed.jstree", data)
    // })

    // setTimeout(async () => {
    //     await shipJsTree.refresh()
    // }, 6000)

    // setTimeout(async () => {
    //     shipJsTree.collapseAll()
    // }, 5000)

});