import { ShipJsTreeOptions, isShipJsTreeOptions, Locale} from "@/types/ShipJsTree";
// Libraries
/**
 * Vue
 */
import { createApp } from 'vue'
import App from './App.vue'
import { registerPlugins } from "@/plugins";

/***
 * Bootstrap
 */
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.min.js'

/**
 * Stores
 */
import { useConfigStore } from '@/stores/ConfigStore'
import { useTreeStore } from '@/stores/TreeStore'

export default class ShipJsTree {
  private options: ShipJsTreeOptions;
  private treeStore: ReturnType<typeof useTreeStore> | undefined;
  private configStore: ReturnType<typeof useConfigStore> | undefined;

  constructor(options: ShipJsTreeOptions) {

    const isShipJsTreeOptionsResult = isShipJsTreeOptions(options)
    if (isShipJsTreeOptionsResult !== true) {
      console.log("options", options)
      console.log("Invalid options in ShipJsTree constructor: "+ isShipJsTreeOptionsResult, " using options: ", options)
      throw new Error('Invalid options in ShipJsTree constructor: ' + isShipJsTreeOptionsResult)
    }
    this.options = options
  }

  async init() {
    const app = createApp(App)

    registerPlugins(app);
    
    this.configStore = useConfigStore()
    this.configStore.setOptions(this.options)
    
    app.mount(`#${this.options.treeId}`)

    this.treeStore = useTreeStore() 
    await this.treeStore.resolveInit()
  }

  on(event: string, callback: Function) {
    this.treeStore?.on(event, callback)
  }

  getPath() {
    return this.treeStore?.getPath()
  }

  changeLocale(locale: Locale) {
    this.options.backend.locale = locale
    this.configStore?.setOptions(this.options)
  }

  async selectNode(uniqueName: string, jumpToNode: boolean = false) {
    await this.treeStore?.selectNode(uniqueName, jumpToNode)
  }

  deselectNode() {
    this.treeStore?.deselectNode()
  }

  getSelectedNode() {
    return this.treeStore?.getSelectedNode()
  }

  search(searchTerm: string) {
    this.treeStore?.search(searchTerm)
  }

  getTree() {
    return this.treeStore?.getTree()
  }

  async refresh() {
    await this.treeStore?.refresh()
  }

  async deleteState() {
    await this.treeStore?.deleteState()
  }

  collapseAll() {
    this.treeStore?.collapseAll()
  }
}