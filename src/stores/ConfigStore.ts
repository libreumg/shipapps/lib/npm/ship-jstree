import { defineStore } from "pinia";
import { ref } from "vue";

import { isShipJsTreeOptions, ShipJsTreeOptions } from "@/types/ShipJsTree";

export const useConfigStore = defineStore("config", () => {
    const options = ref({} as ShipJsTreeOptions);

    function setOptions(newOptions: ShipJsTreeOptions) {
        const isShipJsTreeOptionsResult = isShipJsTreeOptions(newOptions)
        if (isShipJsTreeOptionsResult !== true) {
            console.log("Invalid options in setOptions: "+ isShipJsTreeOptionsResult, " using options: ", newOptions)
            throw new Error('Invalid options in setOptions: ' + isShipJsTreeOptionsResult)
        }
        options.value = {...newOptions}
    }

    function getOptions() {
        return options.value;
    }

    return {
        options,
        setOptions,
        getOptions,
    };
});