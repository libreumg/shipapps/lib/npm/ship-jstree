import { defineStore, storeToRefs } from "pinia";
import { computed, ref, watch } from "vue";
import { useConfigStore } from '@/stores/ConfigStore';
import { JsTreeObj } from '@/types/ShipJsTree';
import { JsonChild, isJsonChild } from '@/types/RestApi';
import $ from "jquery";
import "jstree";
import 'jstree/dist/themes/default/style.css'
import '@/assets/custom-jstree-theme.css'
import axios from "axios";


import select2 from "select2";
import "select2/dist/css/select2.min.css";
import 'select2-bootstrap-5-theme/dist/select2-bootstrap-5-theme.min.css'

(select2 as any)($);

export const useTreeStore = defineStore("tree", () => {
    const treeRef = ref(undefined as JQuery<HTMLElement> | undefined);
    const nodesRef = ref([] as JsTreeObj[]); // used to restore the state
    const isInitializedRef = ref(false);
    const isLoadingRef = ref(false);
    const pathRef = ref("");
    const configStore = ref(useConfigStore());
    const { options } = storeToRefs(configStore.value)

    const baseURL = computed (() => `${options.value.backend.baseUrl}/${options.value.backend.apiVersion}/${options.value.backend.locale}`);
    const api = computed (() => axios.create({ baseURL: baseURL.value ,timeout: 2000 }));

    watch(options, async () => { 
        if(!isInitialized()) return;
        nodesRef.value = [];
        await refresh();
        console.log("options changed, refresh tree and save state")
        saveState();
    })

    async function init(treeId: string): Promise<void> {
        treeRef.value = $(`#${treeId}`);

        setupEventListeners();
        setupExternalElements();

        treeRef.value.jstree({
            core: {
                data: async (node: JsTreeObj, cb: Function) => {
                    if(nodesRef.value.length > 0 && !node.parent) {
                        cb(nodesRef.value);
                        return;
                    }
                    const jsTreeChilds = await getChildren(node);
                    cb(jsTreeChilds);
                },
                force_text: true,
                check_callback : true
            },
            multiple: false,
            types: {
                group: {
                icon: options.value.nodeIconClass,
                },
                variable: {
                icon: options.value.leafIconClass,
                },
            },
            search: {
                ajax: async (nodeId: string, cb: Function) => {
                    const url = baseURL.value + options.value.backend.routes.path + (nodeId === "#" ? "" : `/${nodeId}`);
                    const data = await api.value.get(url).then((res) => res.data);
                    cb(data);
                },
                search_callback: (id: string, node: JsTreeObj) => {
                if (node.id != id) return false;
                return true;
                },
            },
            sort: (a: string, b: string) => {
                const aOrderNr = treeRef.value?.jstree(true).get_node(a).original.orderNr;
                const bOrderNr = treeRef.value?.jstree(true).get_node(b).original.orderNr;

                if (aOrderNr < bOrderNr) return -1;
                if (aOrderNr > bOrderNr) return 1;
                return 0;
            },
            plugins: [
                "types", 
                "search", 
                "sort"
            ],
        } as unknown as JSTreeStatic);
        await resolveInit();
        saveState();
        
        console.log("tree initialized, window.location.hash: ", window.location.hash)
        const hash = window.location.hash;
        const nodeId = hash.substring(1);
        // if the path is not empty, select the node
        if (nodeId !== "") await selectNode(nodeId, true);


        isInitializedRef.value = true;
    }

    /**
     * Sets up event listeners for the tree
     */
    function setupEventListeners(): void {
        if(!treeRef.value) return;

        treeRef.value.on("changed.jstree", () => {
            setPath();
        });

        const saveStateEvents = [
            "open_node.jstree", 
            "close_node.jstree", 
            "changed.jstree",
            "refresh.jstree",
            "create_node.jstree",
            "delete_node.jstree",
            "move_node.jstree",
            "rename_node.jstree",
            "copy_node.jstree",
            "paste_node.jstree",
            "activate_node.jstree",
            "deactivate_node.jstree",
        ]

        treeRef.value.on(saveStateEvents.join(" "), () => {
            saveState();
        });
    }

    /***
     * Sets up event listeners and actions for external elements
     */
    function setupExternalElements(): void {
        if(!treeRef.value) return;

        $(`#${options.value.collapseBtnId}`).on("click", () => collapseAll());

        $(`#${options.value.refreshBtnId}`).on("click", async () => await refresh());

        $(`#${options.value.treeFilterId}`).select2({
            ajax: {
              url: params => baseURL.value + options.value.backend.routes.filter + (params.term ? `/${params.term}` : ""),
              dataType: 'json',
              delay: 250,
              processResults: (data) => {
                return {
                  results: data.map((node: JsonChild) => {
                    return {
                      id: node.uniqueName,
                      text: node.uniqueName,
                    }
                  })
                };
              },
            },
            theme: 'bootstrap-5',
          });
    }

    /***
     * collapse all nodes in the tree
     */
    function collapseAll(): void {
        if(!treeRef.value) return;
        treeRef.value.jstree("close_all");
    }

    /***
     * Sets the nodesRef to the current state of the tree
     */
    function saveState(): void {
        if(!treeRef.value) return
        nodesRef.value = getTree();
    }

    /***
     * set path to the selected node
     */
    function setPath(): void {
        if(!treeRef.value) return;
        const selected = treeRef.value.jstree("get_selected");
        pathRef.value = selected.length ? "/" + treeRef.value.jstree(true).get_path(selected, "/") : "";
    }

    /***
     * get path to the selected node
     */
    function getPath(): string {
        if(!treeRef.value) return "";
        return pathRef.value;
    }

    /***
     * load path to a node
     * 
     * @param uniqueName the uniqueName of the node to load the path to
     */
    async function loadPathToNode(uniqueName: string): Promise<void> {
        if(!treeRef.value) return;
        if(isLoadingRef.value) return;

        isLoadingRef.value = true;

        const route = `${options.value.backend.routes.subtree}/${uniqueName}`;
        const unorderedJsonChilds = await api.value.get(route).then((res) => Object.values(res.data as JsonChild[]));
        unorderedJsonChilds.forEach((node) => {
            const isJsonChildResponse = isJsonChild(node);
            if(isJsonChildResponse !== true) {
                console.error(`Invalid JsonChild in ${route} using node `, node, " error: ", isJsonChildResponse)
                throw new Error(`Invalid JsonChild in ${route}`);
            }
        })

        const unorderedSubtree = unorderedJsonChilds.map(createJsTreeObj)

        const rootNode = unorderedSubtree.find((node) => !node.parent);

        if(!rootNode) {
            console.error(`No root node found in ${route}: `, unorderedSubtree)
            throw new Error(`No root node found in ${route}`);
        }

        const findAndAddChilds = (parent: JsTreeObj) => {
            const children = unorderedSubtree.filter((child) => child.parent === parent.id);
            children.sort((a, b) => a.orderNr - b.orderNr);

            const isInPath = children.length > 0
            
            parent.state.opened = isInPath;
            parent.state.loaded = isInPath;

            parent.children = children;

            children.forEach((child) => {
                findAndAddChilds(child);
            })
        }

        deselectNode();
        findAndAddChilds(rootNode);

        const rootNodePos = treeRef.value.jstree(true).get_node('#').children.indexOf(rootNode.id)

        treeRef.value.jstree("delete_node", rootNode.id)
        treeRef.value.jstree("create_node", '#', rootNode, rootNodePos, false, false);

        setPath();

        isLoadingRef.value = false;
    }



    /***
     * Refreshes the tree
     */
    async function refresh(forgetState = false): Promise<void> {
        if(!treeRef.value) return;
        if(isLoadingRef.value) return;

        isLoadingRef.value = true;
        treeRef.value.jstree("refresh", true, forgetState);
        await new Promise((resolve) => {
            treeRef.value?.on("refresh.jstree", () => {
                isLoadingRef.value = false;
                resolve(true);
            })
        })
    }

    /**
     * Wraps all event listeners for the tree
     */
    function on(event: string, callback: Function): void {
        if(!treeRef.value) return;
        treeRef.value.on(event, (...args) => callback(...args));
    }

    /**
     * Creates and returns jsTree readable node object
     * @param node the JsonChild or JsonProperties object to create the object from.
     * 
     * @returns jsTree readable node object
     */
    function createJsTreeObj(node: JsonChild): JsTreeObj {
        let nodeText;
        if (typeof options.value.nodeTextProperty === "function") {
            nodeText = options.value.nodeTextProperty(node);
        } else {
            nodeText = node[options.value.nodeTextProperty ?? 'uniqueName'] ?? node.uniqueName;
        }

        return {
            id: node.uniqueName,
            parent: node.parentUniqueName ?? undefined,
            text: `${nodeText}`,
            state: {
                opened: false,
                selected: false,
            },
            orderNr: node.orderNr ?? 0,
            children: node.hasChildren,
            type: node.isVariable ? "variable" : "group",
        };
    }
    
    /**
     * Returns a promise that resolves when the tree is fully initialized
     */
    async function resolveInit(): Promise<void> {
        await new Promise((resolve) => {
            resolve(true);
            treeRef.value?.on("ready.jstree", () => {
                resolve(true);
            })
        })
    }

    /**
     * Returns true if the tree is fully initialized
     * @returns true if the tree is fully initialized
     */
    function isInitialized(): boolean {
        return isInitializedRef.value;
    }

    /**
     * Returns true if the tree is loading
     * @returns true if the tree is loading
     */
    function isLoading(): boolean {
        return isLoadingRef.value;
    }

    /***
     * Loads and returns the children of the given node
     */
    async function getChildren(node: JsTreeObj): Promise<JsTreeObj[]> {
        const route = options.value.backend.routes.children + (node.id === "#" ? "" : `/${node.id}`);


        const jsonChilds = await api.value.get(route)
        .then((res) => Object.values(res.data as JsonChild[]));

        jsonChilds.forEach((child) => {
            if(!isJsonChild(child)) {
                console.error(`Invalid JsonChild in ${route}: `, child)
                throw new Error(`Invalid JsonChild in ${route}`);
            }
        })
        const jsTreeChilds = jsonChilds.map(createJsTreeObj)
        return jsTreeChilds;
    }

    /**
     * Selects the node with the given uniqueName
     * 
     * @param uniqueName selects the node with the given uniqueName
     */
    async function selectNode(uniqueName: string, jumpToNode: boolean = false): Promise<void> {
        if(!treeRef.value) return;
        deselectNode();
        const isSelected = treeRef.value.jstree(true).select_node(uniqueName);
        if (isSelected === false) {
            await loadPathToNode(uniqueName);
            treeRef.value.jstree(true).select_node(uniqueName);
        }
        if(jumpToNode) {
            console.log("jump to node: ", uniqueName);
            window.location.hash = ""
            window.location.hash = uniqueName;
        }
    }

    /***
     * Deselects the selected node in the tree
     */
    function deselectNode(): void {
        if(!treeRef.value) return;
        treeRef.value.jstree(true).deselect_all();
    }

    /***
     * clears the search field
     */
    function clearSearch(): void {
        if(!treeRef.value) return;
        treeRef.value.jstree(true).clear_search();
    }

    /**
     * Searches for the given string in the tree
     * 
     * @param searchString the string to search for
     */
    function search(searchString: string): void {
        if(!treeRef.value) return;
        clearSearch();
        treeRef.value.jstree(true).search(searchString);
    }

    /**
     * Returns the selected node
     * 
     * @returns the selected node
     */
    function getSelectedNode(): JsTreeObj | false {
        if(!treeRef.value) return false;
        const selected = treeRef.value.jstree(true).get_selected();
        if (!selected) return false;
        return treeRef.value.jstree(true).get_node(selected[0]);
    }

    /***
     * Deletets the state of the tree
     */
    async function deleteState(): Promise<void> {
        nodesRef.value = [];
        await refresh(true);
        saveState();
    }

    /***
     * Returns the tree
     * 
     * @returns the tree
     */
    function getTree(): JsTreeObj[] {
        if(!treeRef.value) return [];
        return treeRef.value.jstree(true).get_json();
    }

    return {
        // Refs
        nodesRef,

        // Methods
        init,
        isInitialized,
        resolveInit,
        on,
        isLoading,
        getPath,
        refresh,
        selectNode,
        deselectNode,
        clearSearch,
        search,
        getSelectedNode,
        deleteState,
        collapseAll,
        getTree
    };
});
     