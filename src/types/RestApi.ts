export type JsonChild = {
    uniqueName: string;
    translation: string | null | undefined;
    isVariable: boolean;
    orderNr: number | null | undefined;
    hasChildren: boolean;
    parentUniqueName: string | undefined;
};

export function isJsonChild(obj: any): true | string {
    if (typeof obj?.uniqueName !== "string") return "uniqueName is not a string"
    if (typeof obj?.translation !== "string" && typeof obj?.translation !== "undefined" && obj?.translation !== null) return "translation is not a string or undefined or null"
    if (typeof obj?.isVariable !== "boolean") return "isVariable is not a boolean"
    if (typeof obj?.orderNr !== "number" && typeof obj?.orderNr !== "undefined" && obj?.orderNr !== null) return "orderNr is not a number or undefined or null"
    if (typeof obj?.hasChildren !== "boolean") return "hasChildren is not a boolean"
    if (typeof obj?.parentUniqueName !== "string" && typeof obj?.parentUniqueName !== "undefined") return "parentUniqueName is not a string or undefined"
    return true
}