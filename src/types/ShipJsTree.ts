import { JsonChild } from "@/types/RestApi";

export type Locale = "en" | "de";

export type ShipJsTreeOptions = {
    treeId: string;
    treeFilterId: string;

    // Config
    backend: {
        baseUrl: string;
        apiVersion: string;
        locale: Locale;
        routes: {
            children: string;
            subtree: string;
            path: string;
            filter: string;
        }
    };
    saveStateInLocalStorage: boolean;
    
    // Buttons
    collapseBtnId: string;
    refreshBtnId: string;

    // Tree options
    showPath: boolean;
    nodeTextProperty: keyof JsonChild | ((node: JsonChild) => string);
    nodeIconClass: string;
    leafIconClass: string;
};

export type JsTreeObj = {
    // jsTree readable node object
    id: string;
    text: string;
    state: {
        opened: boolean;
        selected: boolean;
        loaded?: boolean;
    };
    orderNr: number;
    children: boolean | JsTreeObj[];
    parent: string | undefined;
    type: "group" | "variable";
};
  
// Type Guard
export function isShipJsTreeOptions(obj: any): true | string {
    if (typeof obj?.treeId !== "string") return "treeId is not a string"
    if (typeof obj?.treeFilterId !== "string") return "treeFilterId is not a string"
    if (typeof obj?.collapseBtnId !== "string") return "collapseBtnId is not a string"
    if (typeof obj?.refreshBtnId !== "string") return "refreshBtnId is not a string"
    if (typeof obj?.nodeTextProperty !== "string" && typeof obj?.nodeTextProperty !== "function") return "nodeTextProperty is not a string or function"
    if (typeof obj?.nodeIconClass !== "string") return "nodeIconClass is not a string"
    if (typeof obj?.leafIconClass !== "string") return "leafIconClass is not a string"
    if (typeof obj?.showPath !== "boolean") return "showPath is not a boolean"
    if (typeof obj?.saveStateInLocalStorage !== "boolean") return "saveStateInLocalStorage is not a boolean"
    const isBackendConfigResult = isBackendConfig(obj?.backend)
    if (typeof isBackendConfigResult === "string") return "backend is not a valid backend config: " + isBackendConfigResult
    return true
}

export function isBackendConfig(obj: any): true | string {
    if (typeof obj?.baseUrl !== "string") return "baseUrl is not a string"
    if (typeof obj?.apiVersion !== "string") return "apiVersion is not a string"
    if (typeof obj?.locale !== "string") return "locale is not a string"
    const isBackendRoutesResult = isBackendRoutes(obj?.routes)
    if (typeof isBackendRoutesResult === "string") return "routes is not a valid backend config: " + isBackendRoutesResult
    return true
}

export function isBackendRoutes(obj: any): true | string {
    if (typeof obj?.children !== "string") return "children is not a string"
    if (typeof obj?.subtree !== "string") return "subtree is not a string"
    if (typeof obj?.path !== "string") return "path is not a string"
    if (typeof obj?.filter !== "string") return "filter is not a string"
    return true
}

export function isJsTreeObj(obj: any): true | string {
    if (typeof obj?.id !== "string") return "id is not a string"
    if (typeof obj?.text !== "string") return "text is not a string"
    if (typeof obj?.state?.opened !== "boolean") return "state.opened is not a boolean"
    if (typeof obj?.state?.selected !== "boolean") return "state.selected is not a boolean"
    if (typeof obj?.orderNr !== "number") return "orderNr is not a number"
    if (!Array.isArray(obj?.children)) return "children is not an array"
    if (obj.type !== "group" && obj.type !== "variable") return "type is not 'group' or 'variable'"
    return true
}
  