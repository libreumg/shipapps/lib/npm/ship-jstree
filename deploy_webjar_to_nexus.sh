#!/bin/bash

if [ -r ~/.nexus_token ] ; then
    if [ -z "$user" -o -z "$secret" -o -z "$nexus" ] ; then
        . ~/.nexus_token
    fi
    if [ -z "$user" -o -z "$secret" -o -z "$nexus" ] ; then
	>&2 echo "Need access to a nexus server, run in CI/CD with variables set or create ~/.nexus_token";
	exit 1;
    fi
fi

if [ -z "$1" ] ; then
    echo "Deploy file to nexus"
    echo "$0 <file>"
    exit 0;
fi

if  [ -z "$2" ] ; then
    echo "second argument missing, assume to use the repository ship-snapshot-java"
    j="ship-snapshot-java"
else
    j=$2
    echo "using repository $j"
fi

# CI_DEFAULT_BRANCH=main # debug code

if [ -z "$CI_DEFAULT_BRANCH" ] ; then
    >&2 echo "Likely not running from gitlab CI/CD -- variables seem not to have been set"
    exit 1
fi

if [ -n "$(git status --porcelain)" ]; then # https://unix.stackexchange.com/a/155077
    >&2 echo "git working tree is not clean"
    >&2 echo " -- should not happen on CI/CD"
    >&2 echo "Aborting"
    exit 1
fi

if [ "$CI_COMMIT_REF_NAME"x != "$CI_DEFAULT_BRANCH"x ] ; then
    >&2 echo "not on branch $CI_DEFAULT_BRANCH"
    >&2 echo " -- won't publish from $CI_COMMIT_REF_NAME"
    >&2 echo "Aborting"
    exit 1
fi

mainbranch=$(git rev-parse origin/$CI_DEFAULT_BRANCH 2> /dev/null) || branch=""

if [ -z "$mainbranch" ] ; then
    >&2 echo "no branch named origin/$CI_DEFAULT_BRANCH found"
    exit 1
fi

# Get the project name and version from the package.json file
PROJECT_NAME=$(grep -o '"name": *"[^"]*"' package.json | awk -F'"' '{print $4}')
PROJECT_VERSION=$(grep -o '"version": *"[^"]*"' package.json | awk -F'"' '{print $4}')

# Get the group from the build.gradle file
PROJECT_GROUP_NAME=$(grep -o 'group .[^"]*' build.gradle | awk -F"'" '{print $2}')

if [ -r "$1" ] ; then
    f="$1"
    e=$(echo "$f" | sed -e "s/.*\.//g")
    de=".$e"
    bn=$(basename $f $de)
    echo "try to publish file $bn of type $e as $PROJECT_NAME on artifact $PROJECT_GROUP_NAME in version $PROJECT_VERSION"
    if (curl -s -X 'GET' \
      "$nexus/service/rest/v1/search/assets?name=$PROJECT_NAME&version=$PROJECT_VERSION&repository=$j" | grep -F $(basename $f)) > /dev/null ; then
      echo $(basename $f) "already exists in $j on $nexus -- won't upload again, force not yet implemented"
      exit 1
    fi
    curl -v -X 'POST' \
      -u "$user:$secret" \
      "$nexus/service/rest/v1/components?repository=$j" \
      -H 'accept: application/json' \
      -H 'Content-Type: multipart/form-data' \
      -F 'maven2.generate-pom=true' \
      -F 'maven2.groupId='"$PROJECT_GROUP_NAME" \
      -F 'maven2.artifactId='"$PROJECT_NAME" \
      -F 'maven2.version='"$PROJECT_VERSION" \
      -F "maven2.asset1=@$1;type=application/x-zip" \
      -F 'maven2.asset1.extension='"$e"
else
    (>&2 echo "Could not find $1")
    exit -1
fi

exit 0;